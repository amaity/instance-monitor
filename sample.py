# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Command-line skeleton application for Compute Engine API.
Usage:
  $ python sample.py

You can also get help on all the command-line flags the program understands
by running:

  $ python sample.py --help

"""

import argparse
import httplib2
import os
import sys

from apiclient import discovery
from oauth2client import file
from oauth2client import client
from oauth2client import tools

from apiclient.discovery import build


DEFAULT_ZONE = 'us-central1-a'
API_VERSION = 'v1'
GCE_URL = 'https://www.googleapis.com/compute/%s/projects/' % (API_VERSION)
PROJECT_ID = 'datashop-beta'
REGION = 'us-central1'
TARGET_POOL = 'rconsole-pool'

# Parser for command-line arguments.
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[tools.argparser])


# CLIENT_SECRETS is name of a file containing the OAuth 2.0 information for this
# application, including client_id and client_secret. You can see the Client ID
# and Client secret on the APIs page in the Cloud Console:
# <https://cloud.google.com/console#/project/366805567240/apiui>
CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secrets.json')

# Set up a Flow object to be used for authentication.
# Add one or more of the following scopes. PLEASE ONLY ADD THE SCOPES YOU
# NEED. For more information on using scopes please see
# <https://developers.google.com/+/best-practices>.
FLOW = client.flow_from_clientsecrets(CLIENT_SECRETS,
    scope=[
      'https://www.googleapis.com/auth/compute',
      'https://www.googleapis.com/auth/compute.readonly',
      'https://www.googleapis.com/auth/devstorage.full_control',
      'https://www.googleapis.com/auth/devstorage.read_only',
      'https://www.googleapis.com/auth/devstorage.read_write',
    ],
    message=tools.message_if_missing(CLIENT_SECRETS))

def get_instances_in_target_pool(argv):
    flags = parser.parse_args(argv[1:])

    # If the credentials don't exist or are invalid run through the native client
    # flow. The Storage object will ensure that if successful the good
    # credentials will get written back to the file.
    storage = file.Storage('sample.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(FLOW, storage, flags)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Construct the service object for the interacting with the Compute Engine API.
    service = discovery.build('compute', 'v1', http=http)

    try:
        print "Success! Now add code here."

    except client.AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
        "the application to re-authorize")


    #Build the service 
    gce_service = build('compute', API_VERSION)
    project_url = '%s%s' % (GCE_URL, PROJECT_ID)
    
    # body ={
    # "instances": [ # URLs of the instances to be added to targetPool.
    #   {
    #     "instance":"https://www.googleapis.com/compute/v1/projects/datashop-beta/zones/us-central1-a/instances/datashop-production-3",
    #   },
    #   {
    #     "instance":"https://www.googleapis.com/compute/v1/projects/datashop-beta/zones/us-central1-a/instances/datashop-mgr"
    #   },
    # ],
    # }
    request = gce_service.targetPools().get(project=PROJECT_ID, region=REGION, targetPool=TARGET_POOL)
    response = request.execute(http=http)
    #response = _blocking_call(gce_service, http, response)

    return response['instances']

def target_pool_add_instance(instances_list, argv):
    flags = parser.parse_args(argv[1:])

    # If the credentials don't exist or are invalid run through the native client
    # flow. The Storage object will ensure that if successful the good
    # credentials will get written back to the file.
    storage = file.Storage('sample.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(FLOW, storage, flags)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Construct the service object for the interacting with the Compute Engine API.
    service = discovery.build('compute', 'v1', http=http)

    try:
        print "Success! Now add code here."

    except client.AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
        "the application to re-authorize")


    #Build the service 
    gce_service = build('compute', API_VERSION)
    project_url = '%s%s' % (GCE_URL, PROJECT_ID)
    body = {
        "instances": instances_list
    }
    # body ={
    # "instances": [ # URLs of the instances to be added to targetPool.
    #   {
    #     "instance":"https://www.googleapis.com/compute/v1/projects/datashop-beta/zones/us-central1-a/instances/datashop-production-3",
    #   },
    #   {
    #     "instance":"https://www.googleapis.com/compute/v1/projects/datashop-beta/zones/us-central1-a/instances/datashop-mgr"
    #   },
    # ],
    # }
    

    request = gce_service.targetPools().addInstance(project=PROJECT_ID, region=REGION, targetPool=TARGET_POOL, body=body)
    response = request.execute(http=http)
    #response = _blocking_call(gce_service, http, response)

    print response





def create_target_pool(argv):
    flags = parser.parse_args(argv[1:])

    # If the credentials don't exist or are invalid run through the native client
    # flow. The Storage object will ensure that if successful the good
    # credentials will get written back to the file.
    storage = file.Storage('sample.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(FLOW, storage, flags)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Construct the service object for the interacting with the Compute Engine API.
    service = discovery.build('compute', 'v1', http=http)

    try:
        print "Success! Code Running starts"

    except client.AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
        "the application to re-authorize")


    #Build the service 
    gce_service = build('compute', API_VERSION)
    project_url = '%s%s' % (GCE_URL, PROJECT_ID)
    target_pool = {
    "sessionAffinity": "CLIENT_IP_PROTO", "name": "lb2-pool", "kind": "compute#targetPool", "resourceType": "targetPools", "healthChecks": [ "https://www.googleapis.com/compute/v1/projects/datashop-beta/global/httpHealthChecks/lb2-check" ], "region": "us-central1", "instances": [ "https://www.googleapis.com/compute/v1/projects/datashop-beta/zones/us-central1-a/instances/rconsole-development" ],  "portRange": "7510", "IPProtocol": "TCP", "name": "lb2-rule", "kind": "compute#forwardingRule", "resourceType": "forwardingRules", "target": "https://www.googleapis.com/compute/v1/projects/datashop-beta/regions/us-central1/targetPools/lb2-pool", "region": "us-central1",  "kind": "compute#httpHealthCheck", "requestPath": "/", "port": 7510, "checkIntervalSec": 5, "timeoutSec": 5, "unhealthyThreshold": 2, "healthyThreshold": 2, "name": "lb2-check", "resourceType": "httpHealthChecks"
    }
    request = gce_service.targetPools().insert(project=PROJECT_ID, region=REGION, body=target_pool)
    response = request.execute(http=http)
    response = _blocking_call(gce_service, http, response)

    print response

def add_instance(argv):
    # Parse the command-line flags.
    flags = parser.parse_args(argv[1:])

    # If the credentials don't exist or are invalid run through the native client
    # flow. The Storage object will ensure that if successful the good
    # credentials will get written back to the file.
    storage = file.Storage('sample.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(FLOW, storage, flags)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Construct the service object for the interacting with the Compute Engine API.
    service = discovery.build('compute', 'v1', http=http)

    try:
        print "Success! Code Running Starts"

    except client.AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
      "the application to re-authorize")


    #Build the service 
    gce_service = build('compute', API_VERSION)
    project_url = '%s%s' % (GCE_URL, PROJECT_ID)

    # List instances
    request = gce_service.instances().list(project=PROJECT_ID, filter=None, zone=DEFAULT_ZONE)
    response = request.execute(http=http)
    if response and 'items' in response:
        instances = response['items']
    for instance in instances:
        print instance['name']
    else:
        print 'No instances to list.'


    #Creating instance
    instance =  {
    'name':'instance-1',
    'machineType': "https://www.googleapis.com/compute/v1/projects/datashop-beta/zones/us-central1-a/machineTypes/n1-standard-1",

    'disks': [{
          'autodelete' : 'true',
          'boot': 'true',
          'type': 'PERSISTENT',
          'initializeParams' : {
            'diskName': 'instance-1',
            'sourceImage': 'https://www.googleapis.com/compute/v1/projects/datashop-beta/global/images/image-rconsole'
        }
    }],
    "networkInterfaces":[
    {
      "network": "https://www.googleapis.com/compute/v1/projects/datashop-beta/global/networks/default", 
      "accessConfigs": [
        { "name": "External NAT", "type": "ONE_TO_ONE_NAT" }
      ]
    }],
    "serviceAccounts": [ { "email": "default", "scopes": [ "https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/compute", "https://www.googleapis.com/auth/devstorage.full_control" ] } ]
    }

    request = gce_service.instances().insert(project=PROJECT_ID, body=instance, zone=DEFAULT_ZONE)
    print gce_service.images().get(project=PROJECT_ID, image='image-rconsole').execute(http=http)
    response = request.execute(http=http)
    response = _blocking_call(gce_service, http, response)

    print response

def delete_instance(argv):
    INSTANCE_TO_DELETE = 'instance-1'

    flags = parser.parse_args(argv[1:])

    # If the credentials don't exist or are invalid run through the native client
    # flow. The Storage object will ensure that if successful the good
    # credentials will get written back to the file.
    storage = file.Storage('sample.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(FLOW, storage, flags)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Construct the service object for the interacting with the Compute Engine API.
    service = discovery.build('compute', 'v1', http=http)

    try:
        print "Success! Code Running Starts"

    except client.AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
        "the application to re-authorize")


    #Build the service 
    gce_service = build('compute', API_VERSION)
    project_url = '%s%s' % (GCE_URL, PROJECT_ID)

    # List instances
    request = gce_service.instances().list(project=PROJECT_ID, filter=None, zone=DEFAULT_ZONE)
    response = request.execute(http=http)
    if response and 'items' in response:
        instances = response['items']
    for instance in instances:
        print instance['name']
    else:
        print 'No instances to list.'


    #Deleting instance


    request = gce_service.instances().delete(project=PROJECT_ID, instance=INSTANCE_TO_DELETE, zone=DEFAULT_ZONE)
    response = request.execute(http=http)
    response = _blocking_call(gce_service, http, response)


    print response

def list_instances(argv):
    # Parse the command-line flags.
    flags = parser.parse_args(argv[1:])

    # If the credentials don't exist or are invalid run through the native client
    # flow. The Storage object will ensure that if successful the good
    # credentials will get written back to the file.
    storage = file.Storage('sample.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(FLOW, storage, flags)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Construct the service object for the interacting with the Compute Engine API.
    service = discovery.build('compute', 'v1', http=http)

    try:
        print "Success! Code Running Starts"

    except client.AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
      "the application to re-authorize")


    #Build the service 
    gce_service = build('compute', API_VERSION)
    project_url = '%s%s' % (GCE_URL, PROJECT_ID)

    # List instances
    request = gce_service.instances().list(project=PROJECT_ID, filter=None, zone=DEFAULT_ZONE)
    response = request.execute(http=http)
    instances_list = []
    if response and 'items' in response:
        instances = response['items']
    for instance in instances:
        print instance['name']
        print instance['selfLink']
        instances_list.append({instance['name']:instance['selfLink']})
    else:
        print 'No instances to list.'

    return instances_list


def _blocking_call(gce_service, auth_http, response):
    """Blocks until the operation status is done for the given operation."""

    status = response['status']
    while status != 'DONE' and response:
        operation_id = response['name']

    # Identify if this is a per-zone resource
    if 'zone' in response:
        zone_name = response['zone'].split('/')[-1]
        request = gce_service.zoneOperations().get(
          project=PROJECT_ID,
          operation=operation_id,
          zone=zone_name)
    else:
        request = gce_service.globalOperations().get(
           project=PROJECT_ID, operation=operation_id)

    response = request.execute(http=auth_http)
    if response:
        status = response['status']
    return response



# For more information on the Compute Engine API you can visit:
#
#   https://developers.google.com/compute/docs/reference/latest/
#
# For more information on the Compute Engine API Python library surface you
# can visit:
#
#   https://developers.google.com/resources/api-libraries/documentation/compute/v1/python/latest/
#
# For information on the Python Client Library visit:
#
#   https://developers.google.com/api-client-library/python/start/get_started
if __name__ == '__main__':
  main(sys.argv)
