import sample
import sys
import time
if __name__ == '__main__':

    while True:
        
        list_instances = sample.list_instances(sys.argv)
        #print list_instances
        
        #instances with prefix of rconsole-dev-
        instance_rconsole_dev = []

        #get the instances in the target pool
        instances_target_pool = sample.get_instances_in_target_pool(sys.argv)
        #print instances_target_pool

        for instance in list_instances:
            #print instance.keys()
            #if the instance has a prefix rconsole-dev-
            if 'rconsole-dev-' in instance.keys()[0]:
                #check if the instance is not in the target pool
                if instance[instance.keys()[0]] not in instances_target_pool:
                    instance_rconsole_dev.append({'instance': instance[instance.keys()[0]]})

        print instance_rconsole_dev
        #add the instances which are not in the target pool
        if instance_rconsole_dev:
            sample.target_pool_add_instance(instance_rconsole_dev, sys.argv)
            
        time.sleep(60)